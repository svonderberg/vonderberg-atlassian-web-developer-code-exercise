import React from 'react';

const SpeakerListing = ({
    FirstName: first,
    LastName: last,
    Company: company
}) =>
    <p className="speaker-listing">
        <span className="name">{first} {last}</span>, {company}
    </p>;

export default SpeakerListing;