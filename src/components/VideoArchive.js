import React from 'react';

import TabBar from './TabBar';
import SessionsNav from './SessionsNav';
import SessionContent from './SessionContent';

import { TRACK_TITLES } from '../constants';

import headerLogo from '../assets/header-logo.png';
import titleLogo from '../assets/title-logo.png';

const VideoArchive = ({
    match: { params: { selectedTrackTitle, selectedSessionId } },
    sessionsById,
    sessionIdsByTrackTitle
}) => {
    let sessionsInTrack = []
    if (sessionsById) {
        sessionsInTrack = sessionIdsByTrackTitle[selectedTrackTitle].map(
            sessionId => sessionsById[sessionId]
        );
    }

    return (
        <div className="video-archive">
            <header>
                <div className="logo-container">
                    <a href="https://www.atlassian.com/">
                        <img src={headerLogo} alt="Go to Atlassian.com" />
                    </a>
                </div>
            </header>

            <div className="video-archive-body">
                <div className="video-archive-body-inner">
                    <h1>
                        <img className="title-logo" src={titleLogo} alt="Atlassian Logo" />
                        Summit
                    </h1>

                    <h2>Video Archive</h2>

                    <div className="tab-bar-container">
                        <TabBar
                            items={
                                TRACK_TITLES.map(
                                    title => ({
                                        title,
                                        url: `/${title}/${sessionIdsByTrackTitle[title][0]}`
                                    })
                                )
                            }
                            selectedItem={selectedTrackTitle}
                        />
                    </div>

                    {
                        sessionsById &&
                            <div className="video-archive-content">
                                <div className="sessions-nav-container">
                                    <h3>{selectedTrackTitle}</h3>

                                    <SessionsNav
                                        sessions={sessionsInTrack}
                                        selectedSessionId={selectedSessionId}
                                    />
                                </div>

                                <div className="session-content-container">
                                    <SessionContent
                                        {
                                            ...(selectedSessionId ?
                                                sessionsById[selectedSessionId] :
                                                sessionsInTrack[0])
                                        }
                                    />
                                </div>
                            </div>
                    }
                </div>
            </div>
        </div>
    )
}

export default VideoArchive;
