import React from 'react';
import { Link } from 'react-router-dom';

import SpeakerListing from './SpeakerListing';

const SessionsNav = ({ sessions, selectedSessionId }) =>
    <div className="sessions-nav">
        {sessions.map(
            ({
                Id: sessionId,
                Title: sessionTitle,
                Track: { Title: trackTitle },
                Speakers: sessionSpeakers
            }) =>
                <Link
                    key={sessionId}
                    className={sessionId === selectedSessionId ? 'selected' : ''}
                    to={`/${trackTitle}/${sessionId}`}
                >
                    <h4>{sessionTitle}</h4>
                    {
                        sessionSpeakers.map(
                            speaker =>
                                <SpeakerListing
                                    key={`${speaker.LastName}-${speaker.FirstName}`}
                                    { ...speaker }
                                />
                        )
                    }
                </Link>
        )}
    </div>;

export default SessionsNav;