import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import axios from 'axios';

import VideoArchive from './VideoArchive';

import { TRACK_TITLES } from '../constants';

class App extends Component {
  constructor(props) {
    super(props);

    this.parseSessionsData = this.parseSessionsData.bind(this);

    this.state = {
      sessionsById: null,
      sessionIdsByTrackTitle: null,
      selectedTrackTitle: TRACK_TITLES[0]
    };
  }

  parseSessionsData({ data: { Items: sessions } }) {
    const sessionsById = {};
    const sessionIdsByTrackTitle = TRACK_TITLES.reduce(
      (sessionIdsByTrackTitle, trackTitle) =>
        ({ [trackTitle]: [], ...sessionIdsByTrackTitle }), {}
    );

    for (let session of sessions) {
      const { Id: sessionId, Track: { Title: trackTitle }} = session;

      // ignore sessions whose track is not in the video archive navigation
      if (TRACK_TITLES.includes(trackTitle)) {
        sessionsById[sessionId] = session;
        sessionIdsByTrackTitle[trackTitle].push(sessionId);
      }
    }
  
    this.setState({ sessionsById, sessionIdsByTrackTitle });
  }

  componentDidMount() {
    axios
      .get('/data/sessions.json')
      .then(this.parseSessionsData);
  }

  render() {
    return (
      this.state.sessionsById ?
        <Router>
          <div>
            <Route
              exact={true}
              path="/"
              render={() => (
                <Redirect
                  to={
                    `/${this.state.selectedTrackTitle}/` +
                    `${this.state.sessionIdsByTrackTitle[this.state.selectedTrackTitle][0]}`
                  }
                />
              )}
            />
            <Route
              path="/:selectedTrackTitle/:selectedSessionId"
              render={
                props =>
                  <VideoArchive
                      sessionsById={this.state.sessionsById}
                      sessionIdsByTrackTitle={this.state.sessionIdsByTrackTitle}
                      { ...props }
                  />
              }
            />
          </div>
        </Router> :
        <p>Loading...</p>
    );
  }
}

export default App;
