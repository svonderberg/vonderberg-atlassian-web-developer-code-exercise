import React from 'react';
import { Link } from 'react-router-dom';

const TabBar = ({ items, selectedItem, onItemSelect }) =>
    <div className="tab-bar">
        {items.map(
            ({title, url}) =>
                <Link
                    key={title}
                    className={title === selectedItem ? 'selected' : ''}
                    to={url}
                >
                    {title}
                </Link>
        )}
    </div>;

export default TabBar;