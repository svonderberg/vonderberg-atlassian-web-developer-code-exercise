import React from 'react';
import SpeakerListing from './SpeakerListing';

const SessionContent = ({
    Title: title,
    Description: desc,
    Speakers: speakers
}) =>
    <div className="session-content">
        <h4>{title}</h4>

        <div className="top-speakers-listing">
            {
                speakers.map(
                    (speaker, idx) =>
                        <SpeakerListing key={idx} { ...speaker } />
                )
            }
        </div>

        <p>{desc}</p>

        <a
            className="sessions-video-link"
            href="https://www.youtube.com/watch?v=4B1o26fQTkI"
        >
            See the Q&amp;A from this talk and others here.
        </a>

        <h5>About the speaker{ speakers.length > 1 && 's' }</h5>

        {
            speakers.map(
                ({Biography: bio, ...rest}, idx) =>
                    <div key={idx}>
                        <SpeakerListing { ...rest } />

                        <p>{bio}</p>
                    </div>
            )
        }
    </div>;

export default SessionContent;