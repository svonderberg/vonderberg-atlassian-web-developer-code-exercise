### Atlassian Web Developer Code Exercise - Scott Vonderberg

Clone via git and use `npm run start` to run a local build of the site.

I've added mobile/responsive features for both the track and session navigation. The site should be able to be viewed on device by the ip that Webpack dev server hosts automatically on the local network.

I've used React (via `create-react-app` for quick scaffolding) along with `react-router` for full routing/deeplinking capability as well SASS for styling.